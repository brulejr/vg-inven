/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Jon Brule <brulejr@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import gun from '@/modules/gun'
import store from '@/modules/store'
import _ from 'lodash'

function createProfile (profile, reject) {
  gun.get('profile', ack => {
    if (ack.err) {
      reject(ack.err)
    }
  }).put(
    _.pick(profile, ['email', 'phone', 'firstName', 'lastName']),
    ack => {
      if (ack.err) {
        reject(ack.err)
      }
    }
  )
}

export default {

  login (email, passwd) {
    return new Promise((resolve, reject) => {
      store.commit('setLoading', true)
      const getUser = store.getters.getUser
      getUser.auth(email, passwd, ack => {
        console.log(ack)
        if (ack.err) {
          store.commit('setLoading', false)
          reject(ack.err)
        } else {
          resolve()
        }
      })
    })
  },

  logout () {
    return new Promise((resolve, reject) => {
      store.commit('setLoading', true)
      const getUser = store.getters.getUser
      getUser.leave().then(() => {
        resolve()
      }).catch((error) => {
        store.commit('setLoading', false)
        reject(error)
      })
    })
  },

  signup (profile) {
    return new Promise((resolve, reject) => {
      store.commit('setLoading', true)
      const getUser = store.getters.getUser
      getUser.create(profile.email, profile.passwd, ack => {
        if (ack.err) {
          store.commit('setLoading', false)
          reject(ack.err)
        } else {
          createProfile(profile, reject)
          resolve()
        }
      })
    })
  }

}
